# Descriptif du projet

Ce projet a pour but de déployer l'application ICD Meteo ainsi que les outils de monitoring Prométheus et Grafana, localement en utilisant Kubernetes.

# Clone du projet

```sh
git clone https://gitlab.imt-atlantique.fr/projet_industriel_groupe1/kubernetes.git
```

# Installation de l'environnement

Tout d'abord, si ce n'est pas déjà fait, installez snap :

```sh
sudo snap install microk8s --classic
sudo usermod -a -G microk8s $USER
sudo chown -f -R $USER ~/.kube
su - $USER
```
Vérifiez que le cluster Kubernetes fonctionne correctement :

```sh
microk8s status --wait-ready
```

Vous pouvez récupérer la liste des noeuds (un seul par défaut) :

```sh
microk8s kubectl get nodes
```

Afin de faciliter la suite et de rester vanilla, ajouter l'alias suivant :

```sh
alias kubectl='microk8s kubectl'
echo "alias kubectl='microk8s kubectl'" >> ~/.bashrc
```

Ajoutez la complétion de la commande kubectl :

```sh
echo 'source <(kubectl completion bash)' >>~/.bashrc
bash
```

# Déploiement de l'application

```sh
kubectl apply -f manifests-monitoring/ns-monitoring.yml
kubectl apply -f manifests-weather/registry-credentials.yml
kubectl apply -f manifests-weather/weather.yml
kubectl apply -f manifests-weather/nginx-config.yml
kubectl apply -f manifests-weather/ingress-weather.yml
kubectl apply -f manifests-monitoring/manifests_prometheus.yml
kubectl apply -f manifests-monitoring/manifest_kube_node_exporter.yml
kubectl apply -f manifests-monitoring/manifests_kube_state_metrics.yml
kubectl apply -f manifests-monitoring/manifest_grafana.yml
```

# Visualiser les pods

```sh
kubectl get pods -w
```

# Accéder à l'application

Pour accéder à l'application ICD Meteo et aux outils de monitoring, rendez-vous sur les adresses suivantes :

- ICD Meteo :
```sh
http://localhost:80
```

- Prométheus :
```sh
http://localhost:30000
```

- Grafana :
```sh
http://localhost:32000
```
